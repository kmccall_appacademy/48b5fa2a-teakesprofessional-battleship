require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_accessor :board, :player, :grid

  def initialize(player = HumanPlayer.new("Carl Indigo"), board = Board.new)
    @player = player
    @board = board
  end

  def board
    @board
  end

  def attack(arr)
    @board[arr] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    pos = @player.get_play
    p pos
    attack(pos)
  end
end
