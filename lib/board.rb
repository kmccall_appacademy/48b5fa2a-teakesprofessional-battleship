class Board
  attr_accessor :grid
  def self.default_grid
    default_grid = Array.new(10) {Array.new(10)}
  end

  def initialize(grid= nil)
    @grid = grid || Board.default_grid
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end


  def count
    self.grid.flatten.count {|n| n != nil}
  end

  def empty?(arr= nil)
    if arr == nil
      if self.count == 0
        return true
      end
    elsif grid[arr[0]] [arr[1]] == nil
      return true
    end
    false
  end

  def full?
    if self.count == (grid.length)**2
      return true
    end
      false
  end

  def place_random_ship
    size = grid.count
    rand_1 = rand size
    rand_2 = rand size
    raise "Full House" if self.full?
      unless self.full?
        self.grid[rand_1][rand_2] = :s
      end
    end

    def won?
      !self.grid.flatten.include?(:s)
    end
end
